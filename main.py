import random
from itertools import combinations
from times.serie_a import serie_a
from times.serie_b import serie_b

from prettytable import PrettyTable
pretty_table = PrettyTable()


def calculate_liquid_goals(goal_1, goal_2):
    if goal_1 < goal_2:
        return goal_2 - goal_1
    if goal_1 > goal_2:
        return goal_1 - goal_2
    return goal_1 - goal_2


def check_win(goal_1, goal_2):
    if goal_1 == goal_2:
        return 'tie'
    if goal_1 > goal_2:
        return 't1w'
    if goal_1 < goal_2:
        return 't2w'


def jogar_partida(divisao, time_a, time_b):
    team_1_goals = random.randrange(1, 4, 1)
    team_2_goals = random.randrange(1, 4, 1)
    team_1_name = time_a['time']
    team_2_name = time_b['time']
    who_wins = check_win(team_1_goals, team_2_goals)
    liquid_goals = calculate_liquid_goals(team_1_goals, team_2_goals)

    for index, team in enumerate(divisao):
        if who_wins == 'tie':
            if team['time'] == team_1_name:
                divisao[index]['total_de_pontos'] += + 1
                divisao[index]['gols_pro'] += team_1_goals
                divisao[index]['partidas_jogadas'] += 1
            if team['time'] == team_2_name:
                divisao[index]['total_de_pontos'] += 1
                divisao[index]['gols_pro'] += team_2_goals
                divisao[index]['partidas_jogadas'] += 1

        if who_wins == 't1w':
            if team['time'] == team_1_name:
                divisao[index]['saldo_gols'] += liquid_goals
                divisao[index]['total_de_pontos'] += 3
                divisao[index]['gols_pro'] += team_1_goals
                divisao[index]['partidas_jogadas'] += 1

            if team['time'] == team_2_name:
                divisao[index]['saldo_gols'] -= liquid_goals
                divisao[index]['gols_pro'] += team_2_goals
                divisao[index]['partidas_jogadas'] += 1

        if who_wins == 't2w':
            if team['time'] == team_2_name:
                divisao[index]['saldo_gols'] += liquid_goals
                divisao[index]['total_de_pontos'] += 3
                divisao[index]['gols_pro'] += team_2_goals
                divisao[index]['partidas_jogadas'] += 1

            if team['time'] == team_1_name:
                divisao[index]['saldo_gols'] -= liquid_goals
                divisao[index]['gols_pro'] += team_1_goals
                divisao[index]['partidas_jogadas'] += 1
    return divisao


def format_values_to_rank(value_of_team):
    list_format = []
    list_format.append(value_of_team[0])
    list_format.append(value_of_team[2])
    list_format.append(value_of_team[1])
    list_format.append(value_of_team[3])
    list_format.append(value_of_team[4])
    return list_format


def mount_pretty_table(team, position):
    team.insert(0, position)
    pretty_table.add_row(team)


def get_points(divisao):
    return divisao['total_de_pontos']


def make_ranking(divisao):
    field_names = ["Posição", "Time",
                   "Partidas Jogadas", "Pontuação", "Saldo de Gols", "Gols Pro"]
    pretty_table.field_names = field_names
    rank_value_list = []
    rank = sorted(divisao, key=get_points, reverse=True)
    for values_dict_team in rank:
        values_teams_list = list(values_dict_team.values())
        rank_value_list.append(values_teams_list)
    position = 1
    for value_of_team in rank_value_list:
        format_list = format_values_to_rank(value_of_team)
        mount_pretty_table(format_list, position)
        position += 1
    return pretty_table


def do_combinations(divisao):
    tuple_combinations = list(combinations(divisao, 2))
    return tuple_combinations


def play_several_games(divisao, combinatios):
    for team_match in combinatios:
        time_a, time_b = team_match
        result = jogar_partida(divisao, time_a, time_b)
    return result


def main():
    result_of_combinatios = do_combinations(serie_b)
    result_of_games = play_several_games(serie_b, result_of_combinatios)
    final_ranking = make_ranking(result_of_games)
    print(final_ranking)


main()
